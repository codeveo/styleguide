# Codeveo style guide

Codeveo style guides for open/close-source projects based. Defines formatters and templates. Eclipse formatter is based on google's eclipse formatter with some changes.  

Codeveo Eclipse formatter settings:
* `Indention` -> `Indentation size: 4`
* `Line Wrapping` -> `Function Calls` -> `Qualified Invocations`  
Set `Wrap all elements, except first if not necessary`  
Tick `Force split, even if line shorter than maximum line width`